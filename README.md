# InOffice
[![Build status](https://ci.appveyor.com/api/projects/status/lrxu9e49je2rghf8?svg=true)](https://ci.appveyor.com/project/robert-irribarren/inoffice)
[![MIT license](http://img.shields.io/badge/license-MIT-brightgreen.svg)](http://opensource.org/licenses/MIT)

InOffice is a way to tell if someone is in the office or not via the accelerator on android phones.

  - Get accurate up to date realtime analysis of people in office
  - Magic
### Installation

In Office requires [Node.js](https://nodejs.org/) v4+ to run.

You need Gulp installed globally:

```sh
$ npm i -g gulp
```

```sh
$ git clone [git-repo-url] inoffice
$ cd inoffice
$ npm i -d
$ NODE_ENV=production node app
```
